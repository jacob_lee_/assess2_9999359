﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assess2_9999359
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            var comboBoxSource = new List<String>();
            comboBoxSource.Add("Meeting");
            comboBoxSource.Add("Important Notice");
            comboBoxSource.Add("Upcoming Events");

            comboBoxSubject.ItemsSource = comboBoxSource;
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new MessageDialog("From: " + textBoxUserEmail.Text + "\nSubject: " +comboBoxSubject.SelectedItem + "\nMessage: " + textBoxMessage.Text);
            

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            dialog.Commands.Add(new UICommand { Label = "Reset", Id = 2 });


            var run = await dialog.ShowAsync();

            if ((int)run.Id == 2)
            {
                textBoxDestination.Text = "";
                textBoxMessage.Text = "";
                textBoxUserEmail.Text = "";
                comboBoxSubject.SelectedIndex = -1;
            }

           
        }
    }
}
